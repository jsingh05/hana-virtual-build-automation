This is currently the repo for deploying SuSE VMs for Hana.  There is a single
playbook for deployment ( buildvhana.yml ) and another for post-config 
( configvhana.yml ).  

********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************
A brief run-down of non-automated tasks that occurr before this repo comes into
play

	1.	Network

		IP planning
			subnetting
			accounting

	2.	Storage

		ONTAP configuration
		Provision luns

	3.	Configure UCS domain

	4.	Configure vSphere

********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************

buildvhana.yml
**************

1.	Construct an ansible inventory file with 2 groups.  1 group that is the
	list of node names, and another group that includes the hostname or ip
	of the Netapp management interface.  Each group needs representation
	in group_vars:

	[servers]
	
	vcuser		| VCenter credentials
	vcpass		|
	vmdatacenter	Datacenter name ( within vcenter )
	vmcluster	vSphere cluster name
	vcenter		vcenter hostname / IP
	vmtemplate	VM template to deploy from
	vmdatastore	Datastore where the VM lives

	[storage]

	nasuser		| Netapp credentials
	naspass		| 
	ansible_connection: local

	*** Using ansible-style inventory was experimental, knowing that there
	would be post-config and other config management tasks in the Hana
	space.  The build role should revert to the csv2json/serverlist method
	for initial deployment and be augmented to create or append to 
	ansible-style inventories for post-config and future configuration
	tasks.

2.	Run playbook

*
* Playbook 
*

	# sudo ansible-playbook -i sdc buildvhana.yml

	This implements the 'san' role against the storage group.  This is
	just a data-collecting role at this time, but may be reserved for 
	Netapp/Storage provisioning automation.  Currently uses netapp_facts
	which is home-grown, to collect data via ssh and store them in json
	structures.  

	Next the build role is effected against the build group.  The following
	variables are defined for hana-specific purposes:

	***
	install_url:	The url of the SuSE installation repo.  Currently hard-
	coded to SuSE-12 for SAP SP1 on the dedicated Hana management server

	vmdatastore:	A ternary varible to place even-numbered hostnames on
	'DS2' and odds on 'DS1' which aligns with the in-place manaual load-
	balancing strategy

	The following are vars files within the build role:

	networks.yml:	A list of dicts describing the subnets carved up for
	vHana.  It includes vlan ID, network address, site ( datacenter ), and
	it's 'spec' or purpose.  

	hana.yml:	List of 'aliases' that map a network device name to a
	network.  This should be consistent per vmdatacenter/distributed switch
	***

*
* Tasks
*

	main
		discovernet.yml - Creates networks data structure.  For every alias in
		'resources' ( hana.yml ), grab the IP address from DNS.  The aliases are
		just the standard nomenclature for various hana networks.  

		resources:
	  	- { alias: "data", ethdev: "eth1" }
	  	- { alias: "log", ethdev: "eth4" }
	  	- { alias: "pvt", ethdev: "eth0" }
	  	- { alias: "bk", ethdev: "eth2" }
	  	- { alias: "client", ethdev: "eth3" }

		For example: osel500000
		'networks' gets a dict appended with a definition of each network 
		( osel500000-data, osel500000-log, etc ) and its assigned device name.
		Then the IP address is located within networks.yml to define netmask,
		gateway, vlan.  

		discoversan.yml - Sets facts for 'lifs' and 'exports' by pattern-matching
		the hostname against the facts discovered from san.  This is in order
		to create the local NFS mounts ( Not yet implemented ).  This may be
		done by templating and addendum to /etc/fstab during config, or by 
		templating the autoyast profile.  

	Scratch-space created for building mini-iso

		make-autoyast.yml - This makes a bootable ISO with a built-in autoyast
		profile.  The autoyast is templated for a HANA build, and includes some
		post-config tasks that haven't been converted to ansible yet.  The
		network config files are also templated and dropped into the root fs 
		during build leveraging AutoYaST mechanisms.  ( see autoinst.j2 and
		ifcfg.j2 )  The ISO is then placed on the vmdatastore where the VM is 
		designated to live.

		make-vm.yml - Deploys VM from named template, and attaches build ISO

		boot-installer.yml - Boots VM to begin AutoYaST install

This completes the VM deployment.  When they are built and finished booting
the configvhana.yml playbook is ran.

configvhana.yml
***************

1.	Currently uses same inventory file as buildvhana.yml and simply effects the
	'config' role against the build group.  

	
	# sudo ansible-playbook -i sdc configvhana.yml

*
* Playbook
*

	Effects 'config' role

*
* Tasks
*

	main

	Runs add_ssh_keys_netmon_sysops.sh ( Walmart script ) to place correct
	SSH keys.  This is pretty nasty, but a necessary bandaid until puppet is
	qualified for SLES-12

	Sets up zypper repos ( 8,9,10 broke at some point in time due to a walmart
	change ).  This is also a puppet band-aid

	Installs CentrifyDC from rpm ( should install from repo when that part is 
	fixed ).  

	Joins server to SAP HANA domain

	
********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************
-Manual Hana install

Configure local NFS mounts
	Use templating to augment fstab after build or autoyast during build
Copy SAP Media ( standard NFS mount )
Run KPI
	Storage		Template Storage KPI json files
	Network		Need to define how cluster pairs are qualified in order to 
				template network KPI json files
	Run KPI program via cron job
	Retrive / parse output files
Configure SID users/groups
	Post-config ansible modules or templating autoyast
Install Hana
	Template silent install configuration
	Run install via cron job

********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************
********************************************************************************
-Manual pacemaker implementation


Install HAE bits on both cluster nodes

	# zypper ref -s
	# zypper in -t pattern ha_sles

________________________________________________________________________________
Note: This is already part of the buildvhana.yml playbook 
________________________________________________________________________________

Choose cluster membership networks. You'll need the network address of 2
subnets on which to maintain quorum, and each node's address on that network.
Typical clusters use the 'public/client' network and the 'private network'.

From either node, invoke the HA bootstrap script and follow the prompts. When
it says waiting for cluster, issue ha-cluster-join from the other node.  You
may have to enter root's password several times while key-based auth is setup. 
This should form a basic cluster with no resources.
________________________________________________________________________________
NODE-A # ha-cluster-init
WARNING: No watchdog device found. If SBD is used, the cluster will be unable to start without a watchdog.
  Do you want to continue anyway? [y/N] y
  Enabling sshd.service
  /root/.ssh/id_rsa already exists - overwrite? [y/N] N
  Configuring csync2
  Generating csync2 shared key (this may take a while)...done
  Enabling csync2.socket
  csync2 checking files

Configure Corosync:
  This will configure the cluster messaging layer.  You will need
  to specify a network address over which to communicate (default
  is eth3's network, but you can use the network address of any
  active interface), a multicast address and multicast port.

  Network address to bind to (e.g.: 192.168.1.0) [172.17.109.128]
  Multicast address (e.g.: 239.x.x.x) [239.144.44.237]
  Multicast port [5405] 5458

Configure SBD:
  If you have shared storage, for example a SAN or iSCSI target,
  you can use it avoid split-brain scenarios by configuring SBD.
  This requires a 1 MB partition, accessible to all nodes in the
  cluster.  The device path must be persistent and consistent
  across all nodes in the cluster, so /dev/disk/by-id/* devices
  are a good choice.  Note that all data on the partition you
  specify here will be destroyed.

  Do you wish to use SBD? [y/N] N
WARNING: Not configuring SBD - STONITH will be disabled.
  Enabling hawk.service
    HA Web Konsole is now running, to see cluster status go to:
      https://172.17.109.168:7630/
    Log in with username 'hacluster', password 'linux'
WARNING: You should change the hacluster password to something more secure!
  Enabling pacemaker.service
  Waiting for cluster........done
  Loading initial configuration

Configure Administration IP Address:
  Optionally configure an administration virtual IP
  address. The purpose of this IP address is to
  provide a single IP that can be used to interact
  with the cluster, rather than using the IP address
  of any specific cluster node.

  Do you wish to configure an administration IP? [y/N] N
  Done (log saved to /var/log/ha-cluster-bootstrap.log)
  
________________________________________________________________________________
NODE-B # ha-cluster-join
WARNING: No watchdog device found. If SBD is used, the cluster will be unable to start without a
 watchdog.
  Do you want to continue anyway? [y/N] y

Join This Node to Cluster:
  You will be asked for the IP address of an existing node, from which
  configuration will be copied.  If you have not already configured
  passwordless ssh between nodes, you will be prompted for the root
  password of the existing node.

  IP address or hostname of existing node (e.g.: 192.168.1.1) [] osel501825
  Enabling sshd.service
  Retrieving SSH keys from osel501825
Password:
  /root/.ssh/id_rsa already exists - overwrite? [y/N] N
  No new SSH keys installed
Password:
  Configuring csync2
Password:
Password:
  Enabling csync2.socket
Password:
  Merging known_hosts
WARNING: known_hosts collection may be incomplete
WARNING: known_hosts merge may be incomplete
  Probing for new partitions......done
Password:
Password:
Password:
  Enabling hawk.service
    HA Web Konsole is now running, to see cluster status go to:
      https://172.17.109.171:7630/
    Log in with username 'hacluster', password 'linux'
WARNING: You should change the hacluster password to something more secure!
  Enabling pacemaker.service
  Waiting for cluster....done
  Done (log saved to /var/log/ha-cluster-bootstrap.log)
________________________________________________________________________________
*
* Automation note
*
*This data already exists in structures created from the build role.  To extend
*automation into HA/Failover implementation, these data should be used to 
*template /etc/corosync/corosync.conf.  The totem section should looks something
*like this.  The mcastaddr should be commented out and the mcastport should be
*managed in some way so that there aren't 2 clusters using the say mcast port
*on the same subnet.  Basically we have been incrementing them by a random
*number.  Be sure the values transport: udpu and rrpmode: passive are set as
*shown below.
________________________________________________________________________________
totem {
        version:        2
        secauth:        off
        cluster_name:   hacluster
        clear_node_high_bit: yes

        crypto_cipher:  none
        crypto_hash:    none

        interface {
                ringnumber:     0
                bindnetaddr:    172.17.109.128
                #mcastaddr:     239.144.44.237
                member  {
                        memberaddr: 172.17.109.168
                }
                member  {
                        memberaddr: 172.17.109.171
                }
        }
        interface {
                ringnumber:     1
                bindnetaddr:    172.17.109.192
                member {
                        memberaddr: 172.17.109.225
                }
                member  {
                        memberaddr: 172.17.109.226
                }
        }
        mcastport:      5458
        transport:      udpu
        rrp_mode:       passive
        ttl:            1
}

________________________________________________________________________________

 csync2, corosync authkey and hawk need to be automated.  We may either
 automate bootstrap script itself or the separate compontents.  Needs dev time.
________________________________________________________________________________

Restart corosync on both nodes and verify cluster membership and rings

	NODE # systemctl restart corosync
	NODE # crm status
	NODE # corosync-cfgtool -s

________________________________________________________________________________
Last updated: Wed Aug  2 15:56:59 2017          Last change: Wed Aug  2 15:02:23 2017 by haclust
er via crmd on osel501825
Stack: corosync
Current DC: osel501826 (version 1.1.13-10.4-6f22ad7) - partition with quorum
2 nodes and 0 resources configured

Online: [ osel501825 osel501826 ]

Full list of resources:

osel501825:~ # corosync-cfgtool -s
Printing ring status.
Local node ID 739339688
RING ID 0
        id      = 172.17.109.168
        status  = ring 0 active with no faults
RING ID 1
        id      = 172.17.109.225
        status  = ring 1 active with no faults
________________________________________________________________________________

This cluster is online and ready to build a STONITH device and enable it.

vSphere 

Pull down vSphere CLI bundle ( 6.0 or 6.5 )

# wget http://oser500870.wal-mart.com/Build/tgz/VMware-vSphere-CLI-6.0.0-3561779.x86_64.tar.gz

Install perl deps from zypper repos ( This requires the SDK repo to be enabled
which should happen during initial build ).

# zypper in e2fsprogs-devel libopenssl-devel perl-Crypt-OpenSSL-RSA perl-Archive-Zip perl-Try-Tiny perl-Crypt-SSLeay perl-Class-MethodMaker perl-Net-INET6Glue perl-Class-Data-Inheritable 

Setup initial CPAN mirror ( Answer NO to automatic config and choose an http 
mirror from the list )

# export http_proxy=http://gec-proxy-svr.homeoffice.wal-mart.com:8080
# perl -MCPAN -e 'shell'

Install required cpan modules

# cpan Devel::StackTrace Convert::ASN1 Crypt::X509 Exception::Class UUID::Random Path::Class UUID Data::Dump SOAP::Lite

Extract and install vSphere CLI

# tar xvf VMware-vSphere-CLI-6.0.0-3561779.x86_64.tar.gz            
# cd vmware-vsphere-cli-distrib/                                    
# ./vmware-install.pl                                               

Create a credential store for VCenter with access to reboot VMs

# /usr/lib/vmware-vcli/apps/general/credstore_admin.pl add -s <VCenter IP> -u <VCenter User>
# mv ~/.vmware/ /etc                                                

Test STONITH device before configuring cluster resource

# stonith -t external/vcenter VI_CREDSTORE=/etc/.vmware/credstore/vicredentials.xml VI_SERVER=<VCenter IP> HOSTLIST="<NODE-A HOSTNAME>=<NODE-A VCenter Name>;<NODE-B HOSTNAME>=<NODE-B VCenter Name>" pcmk_host_check="static-list" RESETPOWERON=0 -lS

Only when this test is successful should you configure the stonith resource

# crm configure primitive stonith-vsphere stonith:external/vcenter \
	params VI_CREDSTORE=/etc/.vmware/credstore/vicredentials.xml VI_SERVER=<VCenter IP> HOSTLIST="<NODE-A HOSTNAME>=<NODE-A VCenter Name>;<NODE-B HOSTNAME>=<NODE-B VCenter Name>" \
	op monitor interval=300 timeout=180s
# crm configure clone cln_stonith-vsphere stonith-vsphere \
	meta is-managed=true clone-node-max=1 target-role=Started

Enable cluster-stonith:

# crm configure property stonith-enabled=true

