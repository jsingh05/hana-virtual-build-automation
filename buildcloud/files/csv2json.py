#!/usr/bin/python
import sys, getopt
import csv
import json

json_file = 'list.json'
csv_rows = []
with open('serverlist2.csv', 'rU') as csvfile:
    reader = csv.DictReader(csvfile)
    title = reader.fieldnames
    for row in reader:
        csv_rows.extend([{title[i]:row[title[i]] for i in range(len(title))}])
    with open(json_file, "w") as f:
        f.write(json.dumps(csv_rows, sort_keys=False, indent=4, separators=(',', ': '),encoding="utf-8",ensure_ascii=False))
    print title
