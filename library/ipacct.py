#!/usr/bin/env python

import os
import sys
import getopt
from pprint import pprint
from ipcalc import Network
from socket import gethostbyaddr

def main(argv):
	dead, alive, dnet, consumed = [], [], {}, ''
	ping = False
	try:
		opts, args = getopt.getopt(argv,"pn:")
	except getopt.GetoptError:
		print 'ipacct.py -n <Network/CIDR> [ -p ]'
		sys.exit(2)	
	for opt, arg in opts:
		if opt == '-p':
			ping = True
		elif opt == '-n':
			subnet = arg
	scopenet = Network(subnet)
	for ip in scopenet:
		try:
			aname = gethostbyaddr(str(ip))
		except:
			aname = ( None, None, [str(ip)] )
		if ping:
			pingy = os.system('ping -c 1 -W 2 ' + str(ip) + '>/dev/null 2>&1')
			if pingy == 0:
				alive.append((str(ip), aname[0]))
			else:
				dead.append((str(ip), aname[0]))
	dnet['network'] = str(scopenet)
	dnet['size'] = scopenet.size()
	dnet['first host'] = scopenet.host_first()
	dnet['last host'] = scopenet.host_last()
	dnet['alive'] = alive
	dnet['dead'] = dead
	pprint(dnet)

if __name__ == "__main__":
	main(sys.argv[1:])
