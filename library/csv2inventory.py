#!/usr/bin/env python

import os
import sys
import csv
import json

def inventory(Host=None):
	nodes = []
	nodevars = []
	invcsv = os.environ.get('CSV_INVENTORY', 'inventory.csv')
	with open(invcsv, 'r') as F:
		reader = csv.DictReader(F)
		for line in reader:
			nodes.append(line['hostname'])
			if line.pop('hostname') == Host:
				return line
	return { '': nodes }
	
if __name__ == '__main__':

	if sys.argv[1] == "--list":
		i=inventory()
	if sys.argv[1] == "--host":
		i=inventory(sys.argv[2])

	print(json.dumps(i))
