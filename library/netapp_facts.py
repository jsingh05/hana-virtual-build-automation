#!/usr/bin/env python

import os
import re
import json
import paramiko
from io import StringIO
from csv import DictReader
from ansible.module_utils.basic import *

Filters = [
           (re.compile(r'^.*\n(?=Vserver)', re.DOTALL), '' ),
           (re.compile(r'\r\n\r\n.*', re.DOTALL), ''),
           (re.compile(r',\r'), r'\r'),
           (',$', '')
          ]


def TableDict(strTable):
    facts = []
    T = StringIO(strTable)
    reader = DictReader(T)

    for row in reader:
        facts.append(row)

    return facts	
	

def Filter(strDirty, reFilters):
    strClean = strDirty

    for F in reFilters:
        strFilter = re.sub(F[0], F[1], strClean)	
        strClean = strFilter

    return strClean


def ssh_cmd(hostname, username, password, command):
    try:
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.WarningPolicy())

        client.connect(hostname, port=22, username=username, password=password)

        stdin, stdout, stderr = client.exec_command(command)
        results = stdout.read().decode("utf-8")
        return results

    finally:
        client.close()
	
def main():

    fields = {
              'hostname': {'required': True, 'type': 'str' },
              'username': {'required': True, 'type': 'str' },
              'passwd': {'required': True, 'type': 'str' }
              }

    module = AnsibleModule(argument_spec=fields)

    params = module.params

    setSeparator = 'set -showseparator "," -rows 0; '
    
    lifsCommand = setSeparator + 'network interface show -fields lif,address'
    lifs_raw = ssh_cmd(params['hostname'],
                       params['username'],
                       params['passwd'],
                       lifsCommand
                      )

    volsCommand = setSeparator + 'volume show -fields volume,policy,junction-path' 
    vols_raw = ssh_cmd(params['hostname'],
                       params['username'],
                       params['passwd'],
                       volsCommand
                      )

    qtreeCommand = setSeparator + 'volume qtree show -fields volume,Qtree'
    qtree_raw = ssh_cmd(params['hostname'],
                       params['username'],
                       params['passwd'],
                       qtreeCommand
                      )

    lifTable = Filter(lifs_raw, Filters)
    lifacts = TableDict(lifTable)

    volTable = Filter(vols_raw, Filters)
    volfacts = TableDict(volTable)

    qtreeTable = Filter(qtree_raw, Filters)
    qtreeFacts = TableDict(qtreeTable)

    module.exit_json(changed=False, lifs=lifacts, vols=volfacts, qtree=qtreeFacts)

if __name__ == '__main__':

    main()
