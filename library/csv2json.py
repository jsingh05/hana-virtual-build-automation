#!/usr/bin/python

from ansible.module_utils.basic import *
import json
import csv

output = []


def main():

    fields = {
        "csvfile": {
            "requried": False,
            "type": "str",
            "default": "/tmp/csvfile.csv"
        },
    }

    module = AnsibleModule(argument_spec=fields)

    params = module.params

    csvfile = params['csvfile']


    csv2json = []

    with open(csvfile, 'r') as f:

        reader = csv.DictReader(f)
        for line in reader:
            csv2json.append(line)

    module.exit_json(changed=False, serverlist=csv2json)

if __name__ == '__main__':
    main()
