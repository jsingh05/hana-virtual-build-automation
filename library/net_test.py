#!/usr/bin/env python

import sys, paramiko

hostname = 'ecahanana01.wal-mart.com'
username = 'Homeoffice\\jahec1'
password = 'Zettabyte1!'
#command = 'set -showseparator "," -rows 0; network interface show -fields lif,address' 

setSeparator = 'set -showseparator "," -rows 0; '

def ssh_cmd(hostname,username,password,command):
    try:
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.WarningPolicy())
    
        client.connect(hostname, port=22, username=username, password=password)

        stdin, stdout, stderr = client.exec_command(command)
        #print(stdout.read())
        return stdout.read()

    finally:
        client.close()


lifsCommand = setSeparator + 'network interface show -fields lif,address'
lifs_raw = ssh_cmd(hostname,
                   username,
                   password,
                   lifsCommand
                  )
print(lifs_raw)

volsCommand = 'volume show -fields volume,policy,junction-path'
vols_raw = ssh_cmd(hostname,
                   username,
                   password,
                   volsCommand
                  )
print(vols_raw)

qtreeCommand = setSeparator + 'volume qtree show -fields volume,Qtree'
qtree_raw = ssh_cmd(hostname,
                    username,
                    password,
                    qtreeCommand
                   )
print type(qtree_raw)

