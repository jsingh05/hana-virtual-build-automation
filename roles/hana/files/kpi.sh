#!/bin/bash

cd /hana/shared && \
	rsync -avp /SAP_Media/build/hwcct . && \
	cd hwcct && \
	source envprofile.sh || exit 1

./hwval -f ../KPI_CFG.json | tee /var/tmp/kpi.out || exit 1

tar cvf /var/tmp/kpi_report.tar report_KPI*

rm -rf report_KPI*

exit 0
