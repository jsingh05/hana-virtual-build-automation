#!/bin/sh
MYDATE=`date +%Y.%m.%d.%H.%M.%S`

ROOT_DIR=`awk -F: '/^root:/ {print $6}' /etc/passwd`
SSH_AUTHFILE="${ROOT_DIR}/.ssh/authorized_keys"
#SSH_AUTHFILE="/temp/testfile"
SSHD_CONFIG="/etc/ssh/sshd_config"
#SSHD_CONFIG="/tmp/sshd_testfile"

echo "##################################################################################"
echo "This script will enable access from the Home Office jump boxes to this server."
echo "##################################################################################"
echo ""

#######################################################
# Ensure .ssh directory in place
if [ ! -d ${ROOT_DIR}/.ssh ]
then
	echo "Creating root's .ssh directory."
#	mkdir -Z system_u:object_r:ssh_home_t:s0 ${ROOT_DIR}/.ssh 
	mkdir -m 0700 ${ROOT_DIR}/.ssh
#	chcon -R unconfined_u:object_r:ssh_home_t:s0 ${ROOT_DIR}/.ssh
	restorecon -v ${ROOT_DIR}/.ssh

fi

#######################################################
# Add ssh keys to root's authorized keys file:
if [ -f  ${SSH_AUTHFILE} ]
then
	echo "Making backup copy of ${SSH_AUTHFILE} in /tmp/"
	cp ${SSH_AUTHFILE} /tmp/root.authorized_keys.${MYDATE}
fi

echo "Adding ssh keys to root's authorized_keys file"

cat <<EOT >> ${SSH_AUTHFILE}
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAteBZvfyKYQyWfOMI7EvAzNmTSSulbB1oviwEkw4n0jQLIcFpLq+/lOP9Yx8r9AvjjuhSC+ENoDaxgBA3W6zmNaY6FBZtTO8oJv204RZwzQ3uqF7Ax2Frk7KRgl02VoNEROVLTu3x7cLfmwxk9RUJ0gECb4R6nxMqw7mtTPZz8WHq4oU5tZyuEBtTXgRDnyP+w3nSgxj3aCppIj2Kp7w1tTygauSZAewVF54rTk2xogFxu/8wJNZR040l07fm9TZMmZhxOZjjvVN9NQmMDSVWMWdhXWhKY7gYdcdKpJXCmc23sRyPW3Hog3pExyXClsxtFpw1BuRt2c4LGKi6F9QsOQ== oseho@netmon-jump
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAo4cLXT9X1w0WgtrRBVEzeNBifRUbNyaN/4LYTie6WTlVc9VQb5DJttfXO46BbFlUtnOwVhVFe6fkhcfS5GfjFqp7DwVPXSYzJuYb0ZhzAramC+iLXSE1krzJjGUEionv4iK/hsUzaXmdk73ZLcmI28JIs8pbRIhD/q+cwnqBqxF8D+m7UMJ/tje5TRdeH+OI6G24MYyt8VgLhL5v9V1NO1OAO97/oRHMPnO0zzYapZNPvS3gQaG7X0xVh/D3OR8kns3pTTu65w9778PMEiW4L+HJ78pphJOgEgZKqgVvsFDVwSst5m826r8C5u/8KjSBp7d6cN4dmcfCAut3P6HuXw== root@oser000200
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAxNDmuCHjB2hL0h7q5h7Rj+0MeRPX+Q8UmO+7G7jh5JMwCrq4uuBlT1cmtzBZM6pD8LLqTtYBOs40LSpLugbbBq4FV3PUKFtarTOCybGB54jNwryYXvYVij4Eg4THWgx3ZQFk1qCXtEh19JvTI5syas/B3oquk434QBGsHxLtWMfwsb8jGqA+3FazUol1ut1zNDjC0b06E3IPwZGUxAp76DIgJpgN3AXU7iIzbwFwGPKRHwEkNFEoP3jTqB4eHUT7XrS2X6ZoTfW9s3YMWl+ifl+ub49xVKqw7bYjKqqBdhwDwbOGfIxluW8R3pHqqaNYRZTEskspOgR/ics3X6YQbw== oseho@oser000100
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAxNDmuCHjB2hL0h7q5h7Rj+0MeRPX+Q8UmO+7G7jh5JMwCrq4uuBlT1cmtzBZM6pD8LLqTtYBOs40LSpLugbbBq4FV3PUKFtarTOCybGB54jNwryYXvYVij4Eg4THWgx3ZQFk1qCXtEh19JvTI5syas/B3oquk434QBGsHxLtWMfwsb8jGqA+3FazUol1ut1zNDjC0b06E3IPwZGUxAp76DIgJpgN3AXU7iIzbwFwGPKRHwEkNFEoP3jTqB4eHUT7XrS2X6ZoTfW9s3YMWl+ifl+ub49xVKqw7bYjKqqBdhwDwbOGfIxluW8R3pHqqaNYRZTEskspOgR/ics3X6YQbw== oseho@oser000200
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAo4cLXT9X1w0WgtrRBVEzeNBifRUbNyaN/4LYTie6WTlVc9VQb5DJttfXO46BbFlUtnOwVhVFe6fkhcfS5GfjFqp7DwVPXSYzJuYb0ZhzAramC+iLXSE1krzJjGUEionv4iK/hsUzaXmdk73ZLcmI28JIs8pbRIhD/q+cwnqBqxF8D+m7UMJ/tje5TRdeH+OI6G24MYyt8VgLhL5v9V1NO1OAO97/oRHMPnO0zzYapZNPvS3gQaG7X0xVh/D3OR8kns3pTTu65w9778PMEiW4L+HJ78pphJOgEgZKqgVvsFDVwSst5m826r8C5u/8KjSBp7d6cN4dmcfCAut3P6HuXw== root@oser000100
EOT
chmod 0600 ${SSH_AUTHFILE}

#######################################################
# Ensure root login enabled in sshd_config
#echo "Make backup copy of sshd_config in /tmp"
#cp ${SSHD_CONFIG} /tmp/sshd_config.orig.${MYDATE}
#
#echo "Enable root login via ssh"
#sed -i '/PermitRootLogin/d' ${SSHD_CONFIG}
#echo "PermitRootLogin yes" >> ${SSHD_CONFIG}
#
#if [ -f /usr/bin/systemctl ]
#then
#	echo "Check sshd status"
#	systemctl status sshd | tee -a /tmp/sshd.status.${MYDATE}.out
#	echo "Restart sshd"
#	systemctl restart sshd
#else
#	echo "Check sshd status"
#	service sshd status | tee -a /tmp/sshd.status.${MYDATE}.out
#	echo "Restart sshd"
#	service sshd restart
#fi
#	
#
########################################################
########################################################
